from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import LoginUsuario, RegistroUsuario
from api.models import Cliente, Mascota
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core import serializers


# Create your views here.
def login_usuario(request):
    form = LoginUsuario(request.POST or None)

    if form.is_valid():
        data = form.cleaned_data
        user = authenticate(username=data.get("rutCliente"),
                            password=data.get("passCliente"))

        if user is not None:
            login(request, user)
            return redirect("listaMascotas")

    return render(request, "loginUsuario.html", {'form': form, })


def logout_usuario(request):
    logout(request)
    return redirect("listaMascotas")


def registro_usuario(request):
    form = RegistroUsuario(request.POST or None)

    if form.is_valid():
        data = form.cleaned_data
        user = User.objects.get(username=data.get("rutCliente"))

        if user is None:
            user = User(username=data.get("rutCliente"),
                        password=data.get("passCliente"),
                        email=data.get("mailCliente"))
            cli = Cliente(mailCliente=data.get("mailCliente"),
                          passCliente=data.get("passCliente"),
                          nombreCliente=data.get("nombreCliente"),
                          rutCliente=data.get("rutCliente"),
                          fechaCliente=data.get("fechaCliente"),
                          fonoCliente=data.get("fonoCliente"),
                          regionCliente=data.get("regionCliente"),
                          ciudadCliente=data.get("ciudadCliente"),
                          viviendaCliente=data.get("viviendaCliente"))
            user.save()
            cli.save()
            return redirect("listaMascotas")

    return render(request, "registroUsuario.html", {'form': form, })


def lista_rescatados(request):
    estado = "Sin Autentificación"
    if request.user.is_authenticated:
        estado = "Autentificado"

    mascotas = Mascota.objects.all()
    contexto = {
        'mascotas': mascotas,
        'estado': estado,
    }
    return render(request, "listaMascotas.html", contexto)


def base_layout(request):
    template = 'templates/template.html'
    return render(request, template)


def getdata(request):
    mascotas = Mascota.objects.all()
    jsondata = serializers.serialize('json', mascotas)
    return HttpResponse(jsondata)
