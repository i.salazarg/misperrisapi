from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^loginUsuario/$', views.login_usuario, name='loginUsuario'),
    url(r'^login/$', views.login_usuario, name='loginUsuario'),
    url(r'^logoutUsuario/$', views.logout_usuario, name='logoutUsuario'),
    url(r'^logout/$', views.logout_usuario, name='logoutUsuario'),
    url(r'^registroUsuario/$', views.registro_usuario, name='registroUsuario'),
    url(r'^listaMascotas/$', views.lista_rescatados, name='listaMascotas'),
    url(r'^$', views.lista_rescatados, name='listaMascotas'),
]