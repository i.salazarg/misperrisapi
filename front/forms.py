from django import forms
from api.choices import CITY_CHOICES, HOUSE_CHOICES


class LoginUsuario(forms.Form):
    rutCliente = forms.CharField(required=True, label="RUT Cliente")
    passCliente = forms.CharField(widget=forms.PasswordInput(), required=True, label="Contraseña")


class RegistroUsuario(forms.Form):
    rutCliente = forms.CharField(required=True, label="RUT Cliente")
    nombreCliente = forms.CharField(required=True, label="Nombre Completo")
    mailCliente = forms.EmailField(required=True, label="Correo Electronico")
    passCliente = forms.CharField(widget=forms.PasswordInput(), required=True, label="Contraseña")
    fonoCliente = forms.CharField(label="Telefono de Contacto")
    fechaCliente = forms.DateField(required=True, label="Fecha de Nacimiento") #min, max
    ciudadCliente = forms.ChoiceField(required=True, choices=CITY_CHOICES, label="Ciudad")
    viviendaCliente = forms.ChoiceField(required=True, choices=HOUSE_CHOICES, label="Tipo de Vivienda")

# https://docs.djangoproject.com/en/2.1/ref/validators/
