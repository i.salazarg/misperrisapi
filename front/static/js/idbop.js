var dbPromise = idb.open('posts.feeds', 5, function(upgradeDb) {
	upgradeDb.createObjectStore('feeds',{keyPath:'pk'});
});


	//collect latest post from server and store in idb
	fetch('http://127.0.0.1:8000/getdata').then(function(response){
		return response.json();
	}).then(function(jsondata){
		dbPromise.then(function(db){
			var tx = db.transaction('feeds', 'readwrite');
	  		var feedsStore = tx.objectStore('feeds');
	  		for(var key in jsondata){
	  			if (jsondata.hasOwnProperty(key)) {
			    	feedsStore.put(jsondata[key]);	
			  	}
	  		}
		});
	});

	//retrive data from idb and display on page
	var post="";
	dbPromise.then(function(db){
		var tx = db.transaction('feeds', 'readonly');
  		var feedsStore = tx.objectStore('feeds');
  		return feedsStore.openCursor();
	}).then(function logItems(cursor) {
		  if (!cursor) {
		  	document.getElementById('offlinedata').innerHTML=post;
		    return;
		  }
		  for (var field in cursor.value) {
		    	if(field=='fields'){
		    		var feedsData=cursor.value[field];
		    		for(var key in feedsData){
		    			if(key =='nombreMascota'){
		    				var nombre = '<h3>'+feedsData[key]+' - ';
		    			}
		    			if(key =='razaMascota'){
		    				var raza = feedsData[key]+'</h3>';
		    			}
		    			if(key == 'estadoMascota'){
		    				var estado = '<p>'+feedsData[key]+'</p>';
                        }
                        if(key == 'descMastoca'){
		    				var desc = '<p>'+feedsData[key]+'</p>';
		    			}	
		    		}
		    		post=post+'<br>'+nombre+raza+'<br>'+estado+'<br>'+desc+'<br>';
		    	}
		    }
		  return cursor.continue().then(logItems);
		});