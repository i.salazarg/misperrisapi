from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^mascotas/full$', views.MascotaFullView.as_view()),
    url(r'^mascotas/basic$', views.MascotaBasicView.as_view()),
    url(r'^mascotas/$', views.MascotaFullView.as_view()),
    url(r'^$', views.MascotaFullView.as_view()),
]
