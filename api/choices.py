CITY_CHOICES = (
    ('', '--Seleccione una Ciudad--'),
    ('Region de Antofagasta', (
            ('TOC', 'Tocopilla'),
            ('MEJ', 'Mejillones'),
            ('ANT', 'Antofagasta'),
        )),
    ('Region Metropolitana', (
            ('QUI', 'Quilicura'),
            ('REN', 'Renca'),
            ('PRO', 'Providencia'),
        )),
    ('Region de Los Rios', (
            ('COR', 'El Corral'),
            ('PAN', 'Panguipulli'),
            ('VAL', 'Valdivia'),
        )),
)

HOUSE_CHOICES = (
    ('', '--Seleccione un Tipo de Vivienda--'),
    ('CPG', 'Casa con Patio Grande'),
    ('CPP', 'Casa con Patio Pequeño'),
    ('CSP', 'Casa sin Patio'),
    ('DEP', 'Departamento'),
)

PET_CHOICES = (
    ('', '--Seleccione un Estado--'),
    ('RES', 'Rescatado'),
    ('DIS', 'Disponible'),
    ('ADO', 'Adoptado'),
)
