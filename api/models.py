from django.db import models
from django.contrib.auth.models import User
from .choices import CITY_CHOICES, HOUSE_CHOICES, PET_CHOICES


# Create your models here.
class Cliente(models.Model):
    userCliente = models.OneToOneField(User,
                                       on_delete=models.CASCADE)
    fechaCliente = models.DateField()  # min, max
    fonoCliente = models.CharField(max_length=12,
                                   null=True,
                                   blank=True,
                                   default='N/A')
    ciudadCliente = models.CharField(max_length=40,
                                     choices=CITY_CHOICES,
                                     default='')
    viviendaCliente = models.CharField(max_length=40,
                                       choices=HOUSE_CHOICES,
                                       default='')


class Mascota(models.Model):
    idMascota = models.AutoField(primary_key=True)
    nombreMascota = models.CharField(max_length=20)
    razaMascota = models.CharField(max_length=30)
    estadoMascota = models.CharField(max_length=40,
                                     choices=PET_CHOICES,
                                     default='')
    fotoMascota = models.ImageField(upload_to='front/media/mascotas/')
    descMascota = models.TextField(max_length=200)

    def __str__(self):
        return self.nombreMascota + ' - ' + self.razaMascota
