from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Mascota
from .serializers import MascotaFullSerializer, MascotaBasicSerializer


# Create your views here.
class MascotaFullView(APIView):
    def get(self, request):
        mascotas = Mascota.objects.all()
        serializer = MascotaFullSerializer(mascotas, many=True)
        return Response(serializer.data)

    def post(self, request):
        return Response()


class MascotaBasicView(APIView):
    def get(self, request):
        mascotas = Mascota.objects.all()
        serializer = MascotaBasicSerializer(mascotas, many=True)
        return Response(serializer.data)

    def post(self, request):
        return Response()
