from rest_framework import serializers
from .models import Mascota


class MascotaFullSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'idMascota',
            'nombreMascota',
            'razaMascota',
            'estadoMascota',
            'fotoMascota',
            'descMascota',
        )
        model = Mascota


class MascotaBasicSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'nombreMascota',
            'razaMascota',
            'estadoMascota',
        )
        model = Mascota
